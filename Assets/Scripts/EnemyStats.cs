﻿using UnityEngine;
using System.Collections;

public class EnemyStats : MonoBehaviour, IDamageable  {

    [SerializeField]
    float health = 1000;

    [SerializeField]
    float damage = 749;






    public void Damage(float amount)
    {
        health -= amount;
        Debug.Log("health: " + health);
        if (health <= 0)
        {
            Destroy (this.gameObject);
        }
    }
}
