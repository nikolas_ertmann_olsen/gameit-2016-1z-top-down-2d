﻿using UnityEngine;
using System.Collections;
//Note this line, if it is left out, the script won't know that the class 'Path' exists and it will throw compiler errors
//This line should always be present at the top of scripts which use pathfinding
using Pathfinding;
using System.Collections.Generic;
using System.Linq;

public class PathFollower : MonoBehaviour
{
    //The point to move to
    public Transform targetTransform;

    private Seeker seeker;
    //The calculated path
    //The AI's speed per second
    public float speed = 100;
    //The max distance from the AI to a waypoint for it to continue to the next waypoint
    public float nextWaypointDistance = 3;

    [SerializeField]
    private float newPathInterval = 0.5f;


    private List<Vector3> vectorPath;

    //The waypoint we are currently moving towards
    private int currentWaypoint = 0;



    public void Start()
    {
        seeker = GetComponent<Seeker>();
        NewPath();
    }

    private void NewPath()
    {
        //Start a new path to the targetPosition, return the result to the OnPathComplete function
        seeker.StartPath(transform.position, this.targetTransform.position, OnPathComplete);

    }

    public void OnPathComplete(Path p)
    {
        //Debug.Log("Yay, we got a path back. Did it have an error? " + p.error);
        if (!p.error)
        {
            vectorPath = p.vectorPath.ToList();
            //Reset the waypoint counter
            currentWaypoint = 0;
        }

        this.Invoke("NewPath", this.newPathInterval);
    }
    public void Update()
    {
        if (vectorPath == null)
        {
            //We have no path to move after yet
            return;
        }
        if (currentWaypoint >= vectorPath.Count)
        {
           // Debug.Log("End Of Path Reached");
            return;
        }
        //Direction to the next waypoint

        Vector3 target = vectorPath[currentWaypoint];

        this.transform.position = Vector3.MoveTowards(this.transform.position, target, Time.deltaTime * this.speed);

        //Check if we are close enough to the next waypoint
        //If we are, proceed to follow the next waypoint
        if (Vector3.Distance(transform.position, target) < nextWaypointDistance)
        {
            currentWaypoint++;
            return;
        }
    }
}
