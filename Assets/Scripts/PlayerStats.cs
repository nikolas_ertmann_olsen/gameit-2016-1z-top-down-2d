﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class PlayerStats : MonoBehaviour, IDamageable {

    [SerializeField]
    private float health = 1000f;
    [SerializeField]
    private float damage = 1f;
    private Rigidbody2D rb;
    [SerializeField]
    private float tpy;
    [SerializeField]
    private float tpx;

    [SerializeField]
    private float hitRadius = 0.5f;

    [SerializeField]
    private float hitDistance = 0.5f;

    [SerializeField]
    private LayerMask hitLayer;

	
	void Start () 
    {
        rb = GetComponent<Rigidbody2D>();
        
	}
	
	
	void Update () 
    {
        if (health == 0f)
        {
            rb.MovePosition(rb.position = new Vector2(tpx, tpy));
        }

        if (Input.GetKeyDown(KeyCode.R)) 
        {
            rb.MovePosition(rb.position = new Vector2(tpx, tpy));
        }

        if (Input.GetKeyDown(KeyCode.Mouse0)) // Add cooldown check
        {
            // We should fire


            // Play animation


            // Damage things in some time
            this.Invoke("HitThings", 0.2f);
        }
	}


    private void HitThings()
    {
        Collider2D[] insideColliders = Physics2D.OverlapCircleAll(this.transform.position, this.hitRadius, this.hitLayer.value);

        foreach (var collider in insideColliders)
        {
            IDamageable damageable = collider.GetComponent<IDamageable>();

            if (damageable != null)
            {
                damageable.Damage(this.damage);
            }
        }

        RaycastHit2D[] hits = Physics2D.CircleCastAll(this.transform.position, this.hitRadius, this.transform.right, this.hitDistance, this.hitLayer.value);

        foreach (var hit in hits)
        {
            if (insideColliders.Contains(hit.collider))
            {
                continue;
            }

            IDamageable damageable = hit.transform.GetComponent<IDamageable>();

            if (damageable != null)
            {
                damageable.Damage(this.damage);
            }
        }
    }

    private void OnDrawGizmos()
    {
        var from = this.transform.position;
        var to = from + this.transform.right * this.hitDistance;

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(from, this.hitRadius);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(to, this.hitRadius);

        Gizmos.color = Color.cyan;
        Gizmos.DrawLine(from, to);
    }

    public void Damage(float amount)
    {
        health -= amount;

        Debug.Log("player health: " + health);

        if (health <= 0)
        {
            // Spilleren er død! Do something
        }
    }
}

public interface IDamageable
{
    void Damage(float amount);
}