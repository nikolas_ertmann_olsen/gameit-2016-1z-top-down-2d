﻿using UnityEngine;
using System.Collections;

public class TopDown2DMovement : MonoBehaviour {

    [SerializeField]
    private float speed = 6f;


	private new Rigidbody2D rigidbody2D;

    void Awake()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
    }
	
	
	
	void Update () 
    {
	    if (Input.GetKey(KeyCode.W))
        {
            rigidbody2D.transform.position += Vector3.up * Time.deltaTime * speed; 
        }

        if (Input.GetKey(KeyCode.A))
        {
            rigidbody2D.transform.position += Vector3.left * Time.deltaTime * speed;
        }
        
        if (Input.GetKey(KeyCode.S))
        {
            rigidbody2D.transform.position += Vector3.down * Time.deltaTime * speed;
        }
        if (Input.GetKey(KeyCode.D))
        {
            rigidbody2D.transform.position += Vector3.right* Time.deltaTime * speed;
        }

        Vector2 screenPos = Camera.main.WorldToScreenPoint(transform.position);
        Vector2 mousePos = Input.mousePosition;
        Vector2 delta = mousePos - screenPos;


        float angle = Mathf.Atan2(delta.y, delta.x) * Mathf.Rad2Deg;

        transform.rotation = Quaternion.Euler(0, 0, angle);


    }
}
